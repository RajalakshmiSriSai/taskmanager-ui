import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../model/task';
import { Router,ActivatedRoute } from '@angular/router';
import { SharedServiceService } from '../services/shared-service.service';



@Component({
  selector: 'app-updateview',
  templateUrl: './updateview.component.html',
  styleUrls: ['./updateview.component.css']
})
export class UpdateviewComponent implements OnInit {
  item : Task;
  taskItem :Task[];

  constructor(private _router:ActivatedRoute,private service:SharedServiceService) {
    this.item = new Task();
    this._router.params.subscribe(p=>{
      this.item.TaskId=+p['id'];
      })
      this.service.GetTaskById(this.item.TaskId).subscribe((data:Task)=>{
        this.item = data;
        });
        
 }
  ngOnInit() {
    this.service.GetAllTask().subscribe((data:Task[])=>{
      this.taskItem = data;
      });
  }
  UpdateTask(task:Task)
  {
    debugger;
    task.Startdate = new Date (task.StartDateString);
    task.EndDate = new Date (task.EndDateString);
    this.service.UdateTask(task).subscribe(data=>this.item=data);     
  }
  Reset()
  {
    this.item = new Task();
  }
}
