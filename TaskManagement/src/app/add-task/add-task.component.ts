import { Component, OnInit } from '@angular/core';
import { Task } from '../model/task';
import { SharedServiceService } from '../services/shared-service.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  iteam:Task
  taskIteam :Task[];
  constructor(private sharedService:SharedServiceService) { 
  }
  
  ngOnInit() {
    this.sharedService.GetAllTask().subscribe((taskdate:Task[])=>{
    this.taskIteam = taskdate;
    });
    this.iteam = new Task();
  }

  Add(tas:Task)
  {
    this.sharedService.PostTask(tas).subscribe(x=>console.log(x));
  }
  Reset()
  {
    this.iteam = new Task();
  }
}
