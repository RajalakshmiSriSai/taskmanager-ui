import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { Task } from '../model/task';
import { JsonPipe } from '@angular/common';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {
  selectTask :Task;
  taskList:Task[];

  constructor(private http:HttpClient) { }

 
  PostTask(Item:Task):Observable<Task>{

    let result:Observable<Task>;
    
    console.log(Item);
    
    result=this.http.post<Task>("http://localhost:49487/Api/AddTask",Item);
    
    console.log(result);
    
    return result;​
  }

  UdateTask(Item:Task):Observable<Task>{

    let result:Observable<Task>;
    
    console.log(Item);
    
    result=this.http.put<Task>("http://localhost:49487/Api/UpdateTask",Item);
    
    console.log(result);
    
    return result;​
  }
  GetAllTask():Observable<Task[]>
  {
    debugger;
    let result:Observable<Task[]>;
    result= this.http.get<Task[]>("http://localhost:49487/Api/GetAllTask");
    return result;
  }
  GetTaskById(id:number):Observable<Task>
  {
   return this.http.get<Task>("http://localhost:49487/Api/GetTaskById/"+id);
  }
  EndTaskById(id:number)
  {
    return  this.http.delete<Task>("http://localhost:49487/Api/EndTask/"+id);
  }
  Delete(id:number):Observable<Task>
  {
    return  this.http.delete<Task>("http://localhost:49487/Api/DeleteTask/"+id);
  }
}