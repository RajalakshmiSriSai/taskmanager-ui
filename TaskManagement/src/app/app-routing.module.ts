import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddTaskComponent} from './add-task/add-task.component';
import {ViewTaskComponent} from './view-task/view-task.component';
import { UpdateviewComponent } from './updateview/updateview.component';

const routes: Routes = [{path:'',redirectTo:'/addTask',pathMatch:"full"},{path :'addTask',component :AddTaskComponent},
{path :'viewTask',component :ViewTaskComponent},{path :'UpdateTask/:id',component :UpdateviewComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
