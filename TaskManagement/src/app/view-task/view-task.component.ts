import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Task } from '../model/task';
import { SharedServiceService } from '../services/shared-service.service';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {
viewIteam :Task;
ListofTasks: Task[] ;
public nameSearch:string;
public parentTaskSearch:number;
public priorityFromSearch:number;
public priorityToSearch:number;
public startDateSearch:string;
public endDateSearch:string;
  constructor(private _router:Router,private service:SharedServiceService) {
   this. viewIteam = new Task();
 
    
   }
  ngOnInit() {
   this.GetAllTask();
  }
  public  GetAllTask()
  {
    this.service.GetAllTask().subscribe((taskdate:Task[])=>{
      debugger;
      this.ListofTasks = taskdate;
      });
  }
  EndTask(id:number)
 {
  this.service.EndTaskById(id).subscribe(data=>{
    data
      this.GetAllTask();
  });
 }

 Delete(id:number)
  {
    this.service.Delete(id).subscribe(data=>
      {
      data
      this.GetAllTask();
    });
  }

   IsTaskEnd(task:Task)
   {
     if(task.EndDateString == Date.now.toString() )
     {
      return true;
     }
     else{
       return true;
     }
   }
}
